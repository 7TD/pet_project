import ButtonAnimated from '../../components/ButtonAnimated/ButtonAnimated';
import CardAnimated from '../../components/CardAnimated/CardAnimated';
import DatePicker from '../../components/DatePicker/DatePicker';
import LoginForm from '../../components/LoginForm/LoginForm';
import Modal from '../../components/Modal/Modal';
import TextAnimatedGradient from '../../components/TextAnimatedGradient/TextAnimatedGradient';
import styles from './Home.module.scss';
import React from 'react';
import ModalDemonstrate from './components/ModalDemonstrate/ModalDemonstrate';
import { useTypedSelector } from '../../hooks/useTypedSelector';
import { refreshToken } from '../../store/auth/actionCreators';
import { useAppDispatch } from '../../store';

const Home = () => {
    const authData = useTypedSelector(state => state.auth.authData);

    const dispatch = useAppDispatch();

    const [isModalOpenLogin, setIsModalOpenLogin] = React.useState(false);
    const [isModalOpenRegisterl, setIsModalOpenRegister] = React.useState(false);
    const [isModalOpen, setIsModalOpen] = React.useState(false);

    const handleAuthData = () => {
        console.log(authData);
    }

    const handleRefresh = () => {
        dispatch(refreshToken());
    }

    //DatePecker
    const onDateChange = (e: {target: HTMLInputElement}) => {
        console.log(e.target.value)
    }

    //Modal window
    const toggleModal = () => {
        setIsModalOpen(prev => !prev);
    }

    const toggleModalLogin = () =>
        setIsModalOpenLogin(prev => !prev);

    const toggleModalRegister = () =>
        setIsModalOpenRegister(prev => !prev);

    return (
        <div className={styles.Home}>
            <main>
                <section>
                    <h2>
                        Date pecker
                    </h2>
                    <DatePicker
                        id='demo'
                        onChange={onDateChange}
                        placeholder="Please select a date..." 
                    />
                </section>
                <section>
                    <h2>
                        Modal window
                    </h2>
                    <div>
                        <ButtonAnimated value={'open modal window'} onClick={toggleModal} />
                        <Modal isOpen={isModalOpen} onClose={toggleModal}>
                            <ModalDemonstrate />
                        </Modal>
                    </div>
                </section>
                <section>
                    <h2>
                        H1 третьего section
                    </h2>
                    <p>{authData.accessToken}</p>
                    <p>{authData.refreshToken}</p>
                    <button onClick={handleAuthData}>authData</button>
                    <button onClick={handleRefresh}>authRefresh</button>
                </section>
            </main>

            <footer>
                
            </footer>



            {/* <LoginForm /> */}
            <button onClick={toggleModalLogin}>open modal login</button>
            <button onClick={toggleModalRegister}>open modal register</button>

            <Modal isOpen={isModalOpenRegisterl} onClose={toggleModalRegister}>
                <CardAnimated>
                    <TextAnimatedGradient value='Register' />
                    <LoginForm />
                </CardAnimated>
            </Modal>

            <Modal isOpen={isModalOpenLogin} onClose={toggleModalLogin}>
                <CardAnimated>
                    <TextAnimatedGradient value='SIG IN' />
                    <LoginForm />
                </CardAnimated>
            </Modal>
        </div>
    );
}

export default Home;