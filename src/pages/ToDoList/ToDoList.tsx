import ToDOApp from '../../components/ToDoApp/ToDoApp';
import styles from './ToDoList.module.scss';

const ToDoList = () => {


    return (
        <div className={styles.ToDoList}>
            <main>
                <section>
                    <h2>ToDo List</h2>

                    <ToDOApp />
                </section>
                
            </main>
        </div>
    );
}

export default ToDoList;