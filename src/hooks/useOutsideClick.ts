import { RefObject, useEffect } from "react"

const useOutsideClick = (
    ref: RefObject<HTMLElement>,
    callback: () => void,
    opened: boolean
) : void => {

    useEffect(() => {
        const handleClick = (e: MouseEvent) => {
            if (ref.current) {
                const rect = ref.current.getBoundingClientRect()
                const outside = e.clientX < rect.left || e.clientX > rect.right || e.clientY < rect.top || e.clientY > rect.bottom ? true : false
                if (outside) {
                    callback();
                }
            }
        }
        if (opened) {
            setTimeout(() => {document.addEventListener('click', handleClick)})
        } else {
            document.removeEventListener('click', handleClick)
        }
        return () => {
            document.removeEventListener('click', handleClick)
        }
    }, [ref, callback, opened])
}

export default useOutsideClick