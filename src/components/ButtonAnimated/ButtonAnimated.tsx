import React from 'react';
import styles from './ButtonAnimated.module.scss';

interface IButtonAnimated {
    value: string,
    onClick?: () => void;
}

const ButtonAnimated: React.FC<IButtonAnimated> = ({ value, onClick = null }) => {
    const divRef = React.useRef<HTMLButtonElement>(null);
    const [isFocused, setIsFocused] = React.useState(false);
    const [position, setPosition] = React.useState({ x: 0, y: 0 });
    const [opacity, setOpacity] = React.useState(0);

    const handleMouseMove = (e: React.MouseEvent<HTMLButtonElement>) => {
        if (!divRef.current || isFocused) 
            return;

        const div = divRef.current;
        const rect = div.getBoundingClientRect();

        setPosition({ x: e.clientX - rect.left, y: e.clientY - rect.top });
    }

    const handleFocus = () => {
        setIsFocused(true);
        setOpacity(1);
    }

    const handleBlur = () => {
        setIsFocused(true);
        setOpacity(0);
    }

    const handleMouseEnter = () => {
        setOpacity(1);
    }

    const handleMouseLeave = () => {
        setOpacity(0);
    }

    const onClick2 = () => {
        onClick ? onClick() : console.log('123');
    }

    return (
        <div className={styles.Container}>
            <button
                ref={divRef}
                onMouseMove={handleMouseMove}
                onFocus={handleFocus}
                onBlur={handleBlur}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                onClick={onClick2}
                className={styles.ContainerButton}
            >
                <div
                    className={styles.ButtonHover}
                    style={{
                        opacity,
                        background: `radial-gradient(300px circle at ${position.x}px ${position.y}px, rgba(255,255,255,.1), #0000000f)`,
                    }}
                />
                { value }
            </button>
        </div>
    );
}

export default ButtonAnimated;