import styles from './Table.module.scss';
import React from 'react';

interface TableProps {
    data: object[];
    header: string[];
}

const Table: React.FC<TableProps> = ({ data, header }) => {


    return (
        <>
            {header.map((item) => {
                return (
                    <>
                        {item}
                    </>
                )
            })}
        </>
    );
}

export default Table;
