import React from 'react';
import styles from './Modal.module.scss';

interface ModalProps {
    children: React.ReactComponentElement<any>[] | React.ReactComponentElement<any>,
    onClose: () => void;
    isOpen: boolean;
}

const Modal: React.FC<ModalProps> = ({ children, onClose, isOpen }) => {
    const overlayRef = React.useRef(null);

    const handleOverlayClick = (event: React.MouseEvent) => {
        if (event.target === overlayRef.current)
            onClose();
    }

    return isOpen ? (
        <div className={styles.modal}>
            <div className={styles.modalOverlay} onClick={handleOverlayClick} ref={overlayRef} />
            <div className={styles.modalChildren}>
                {children}
            </div>
        </div>
    ) : null;
};

export default Modal;