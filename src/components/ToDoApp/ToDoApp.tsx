import styles from './ToDoApp.module.scss';
import React from 'react';
import TaskField from './components/TaskField/TaskField';
import { ITask, ITaskList } from './types';


const tasks: Array<ITask> = [
    {id: 1, title: 'Learn React', completed: false},
    {id: 2, title: 'Learn Redux', completed: false},
    {id: 3, title: 'Learn MobX', completed: false},
];
const tasks2: Array<ITask> = [
    {id: 4, title: '2Learn React', completed: false},
    {id: 5, title: '2Learn Redux', completed: false},
    {id: 6, title: '2Learn MobX', completed: false},
];
const _taskList: ITaskList[] = [{
    id: 1,
    title: 'First task list',
    tasks: tasks,
},
{
    id: 2,
    title: 'Second task list',
    tasks: tasks2,
},
]

const ToDOApp = () => {
    const [taskList, setTaskList] = React.useState<Array<ITaskList>>(_taskList);

    return (
        <div className={styles.TaskList}>
            {taskList.map((item) => {
                return (
                    <div className={styles.TaskListOne}>
                        <h3>{item.title}</h3>
                        <div className={styles.Tasks}>
                            {item.tasks.map((task) => {
                                return (
                                    <TaskField task={task}></TaskField>
                                );
                            })}
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

export default ToDOApp;

