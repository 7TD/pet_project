
export interface ITask {
    id: number,
    title: string,
    completed: boolean
}
export interface ITaskList {
    id: number,
    title: string,
    tasks: Array<ITask>,
}