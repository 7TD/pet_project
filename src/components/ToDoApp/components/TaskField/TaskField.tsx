import React from 'react';
import { ITask } from '../../types';
import styles from './TaskField.module.scss';

interface TaskFieldProps {
    task: ITask,
}

const TaskField = ({task}: TaskFieldProps) => {
    const [completed, setCompleted] = React.useState<boolean>(task.completed);

    const handleClick = () => {
        setCompleted(prev => !prev);
        task.completed = !task.completed;
    }

    return (
        <div className={styles.TextField}>
            <input type='checkbox' onClick={handleClick} checked={completed} />
            <p onClick={handleClick}>{task.title}</p>
        </div>
    );
}

export default TaskField;