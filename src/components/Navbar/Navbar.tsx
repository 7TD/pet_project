import styles from './Navbar.module.scss';
import React from 'react';

const Navbar = () => {


    return (
        <nav className={styles.Navbar}>
            <a href='/' className={styles.NavbarTitle}>Site Name</a>
            <ul>
                <li><a href='/'>Home</a></li>
                <li><a href='/'>ToDo List</a></li>
                <li><a href='/'>Maybe Home</a></li>
                <li><a href='/'>Probably Home</a></li>
            </ul>
            <div className={styles.NavbarAvatar} />
        </nav>
    );
}

export default Navbar;