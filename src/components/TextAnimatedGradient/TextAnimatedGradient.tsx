import styles from './TextAnimatedGradient.module.scss';

interface ITextAnimatedGradient {
    value: string,
}

const TextAnimatedGradient: React.FC<ITextAnimatedGradient> = ({ value }) => {


    return (
        <div className={styles.Button}>
            <span className={styles.ButtonSpan}>{ value }</span>
        </div>
    );
}

export default TextAnimatedGradient;