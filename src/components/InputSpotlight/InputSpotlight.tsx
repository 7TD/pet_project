import styles from './InputSpotlight.module.scss';
import React, { useState } from 'react';

interface InputProps {
    placeholder: string,
    type?: string,
    name?: string,
    action?: {
        setValue: React.Dispatch<React.SetStateAction<string>>
        startValue?: string,
    }
}

const InputSpotlight = ({ placeholder, type = 'email', name = 'email', action }: InputProps) => {
    const divRef = React.useRef<HTMLInputElement>(null);
    const [isFocused, setIsFocused] = useState(false);
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [opacity, setOpacity] = useState(0);

    const [value, setValue] = React.useState('');

    React.useEffect(() => {
        if (action?.startValue) {
            setValue(action.startValue);
            action.setValue(action.startValue);
        }
    }, []);

    const handleSetValue = (event: any) => {
        setValue(event);
        action && action.setValue(event);
    }

    const handleMouseMove = (e: React.MouseEvent<HTMLElement>) => {
        if (!divRef.current || isFocused) 
            return;

        const div = divRef.current;
        const rect = div.getBoundingClientRect();

        setPosition({ x: e.clientX - rect.left, y: e.clientY - rect.top });
    };

    const handleFocus = () => {
        setIsFocused(true);
        setOpacity(1);
    };

    const handleBlur = () => {
        setIsFocused(false);
        setOpacity(1);
    };

    const handleMouseEnter = () => {
        setOpacity(1);
    };

    const handleMouseLeave = () => {
        setOpacity(0);
    };

    return (
        <div className={styles.Input}>
            <input
                onMouseMove={handleMouseMove}
                onFocus={handleFocus}
                onBlur={handleBlur}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                autoComplete='off'
                placeholder={placeholder}
                type={type}
                name={name}
                className={styles.InputContent}
                value={value}
                onChange={e => handleSetValue(e.target.value)}
            />
            <input
                ref={divRef}
                disabled
                style={{
                    border: '1px solid #8678F9',
                    opacity,
                    WebkitMaskImage: `radial-gradient(30% 30px at ${position.x}px ${position.y}px, black 45%, transparent)`,
                    }}
                aria-hidden='true'
                className={styles.InputAnimated}
            />
        </div>
    );
}

export default InputSpotlight;