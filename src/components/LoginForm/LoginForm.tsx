import styles from './LoginForm.module.scss';
import InputSpotlight from '../InputSpotlight/InputSpotlight';
import ButtonAnimated from '../ButtonAnimated/ButtonAnimated';
import React, { FormEvent } from 'react';
import { loginUser } from '../../store/auth/actionCreators';
import { useAppDispatch } from '../../store';

const LoginForm = () => {
    const dispatch = useAppDispatch();

    const [userName, setUserName] = React.useState('');
    const [password, setPassword] = React.useState('');

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();

        dispatch(loginUser({ userName, password }));
    }

    return (
        <div className={styles.loginForm}>
            <form onSubmit={handleSubmit}>
                <div className={styles.loginFormInput}>
                    <InputSpotlight 
                        placeholder='Login'
                        name='username'
                        type='text'
                        action={{
                            setValue: setUserName,
                            startValue: 'string',
                        }}
                    />
                </div>
                <div className={styles.loginFormInput}>
                    <InputSpotlight 
                        placeholder='Password' 
                        name='password' 
                        type='password'
                        action={{
                            setValue: setPassword,
                            startValue: 'Rercby007@',
                        }}
                    />
                </div>
                <div className={styles.loginFormButton}>
                    <ButtonAnimated
                        value='Login'
                    />
                </div>
            </form>
        </div>
    );
}

export default LoginForm