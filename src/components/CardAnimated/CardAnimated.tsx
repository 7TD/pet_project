import styles from './CardAnimated.module.scss';

interface CardAnimatedProps {
    children: React.ReactComponentElement<any>[] | React.ReactComponentElement<any>,
}

const CardAnimated: React.FC<CardAnimatedProps> = ({ children }) => {
    return (
        <div className={styles.card}>
            <span className={styles.cardSpan} />
            <div className={styles.cardBackground}>
                {children}
            </div>
        </div>
    );
}

export default CardAnimated;