import React, { useRef, useState, useEffect } from 'react';
import LoginForm from './components/LoginForm/LoginForm';
import CardAnimated from './components/CardAnimated/CardAnimated';
import TextAnimatedGradient from './components/TextAnimatedGradient/TextAnimatedGradient';
import Modal from './components/Modal/Modal';
import Navbar from './components/Navbar/Navbar';
import Home from './pages/Home/Home';
import ToDoList from './pages/ToDoList/ToDoList';

function App() {
  const [isModalOpen, setIsModalOpen] = React.useState(false);

  const toggleModal = () =>
    setIsModalOpen(prev => !prev);

  return (
    <>
      <header>
        <Navbar />
      </header>
      <Home></Home>
    </>
  );
}

export default App;
