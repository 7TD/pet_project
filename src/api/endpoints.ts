const Endpoints = {
    AUTH: {
        LOGIN: '/connect/token',
        REGISTER: 'register',
        LOGOUT: '/connect/logout',
        REFRESH: '/connect/token',
        USERINFO: '/connect/userinfo'
    },
}

export default Endpoints;