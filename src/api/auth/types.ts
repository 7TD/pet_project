//register
export interface IRegisterRequest {
    userName: string,
    password: string,
    email: string,
    phoneNumber?: string,
    confirmPassword: string;
}

//login
export interface ILoginRequest {
    userName: string,
    password: string,
}

export interface ILoginRespons {
    access_token: string,
    token_type: string,
    expires_in: number,
    id_token: string,
    refresh_token: string,
}

//refresh
export interface IRefreshResponse {
    access_token: string,
    token_type: string,
    expires_in: number,
    scope: string,
    id_token: string,
    refresh_token: string,
}



