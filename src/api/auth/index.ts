import { AxiosPromise } from "axios";
import { ILoginRequest, ILoginRespons, IRefreshResponse } from "./types";
import Endpoints from "../endpoints";
import { axiosInstance } from "../instance";

export const login = (params: ILoginRequest): AxiosPromise<ILoginRespons> => 
    axiosInstance.post(`https://localhost:7088${Endpoints.AUTH.LOGIN}`, {
        'grant_type': 'password',
        'username': params.userName,
        'password': params.password,
        'scope': 'openid profile email offline_access',
    },
    {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': '*/*',
        },
    },
);

export const refresh = (refreshToken: string): AxiosPromise<IRefreshResponse> => 
    axiosInstance.post(`https://localhost:7088${Endpoints.AUTH.REFRESH}`, {
        'grant_type': 'refresh_token',
        'refresh_token': refreshToken,
    },
    {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': '*/*',
        }
    },
);

