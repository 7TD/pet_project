import { PayloadAction, createSlice } from "@reduxjs/toolkit";

export interface AuthState {
    authData: {
        accessToken: string | null,
        refreshToken: string | null,
        isLoading: boolean,
        error: string | null,
    }
}

interface Tokens {
    accessToken: string,
    refreshToken: string,
}

const initialState: AuthState = {
    authData: {
        accessToken: null,
        refreshToken: null,
        isLoading: false,
        error: null,
    }
}

export const authReducer = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        loginStart: (state): AuthState => ({
            ...state,
            authData: {
                ...state.authData,
                isLoading: true,
            }
        }),
        loginSucess: (state, action: PayloadAction<Tokens>): AuthState => ({
            ...state,
            authData: {
                ...state.authData,
                accessToken: action.payload.accessToken,
                refreshToken: action.payload.refreshToken,
                isLoading: false,
                error: null,
            }
        }),
        loginFailure: (state, action: PayloadAction<string>): AuthState => ({
            ...state,
            authData: {
                ...state.authData,
                isLoading: false,
                error: action.payload,
            }
        }),
        logoutSucess: (): AuthState => initialState,
    }
});

export const { loginStart, loginSucess, loginFailure, logoutSucess } = authReducer.actions;
export default authReducer.reducer;