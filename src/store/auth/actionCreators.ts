import { Dispatch } from "@reduxjs/toolkit";
import api from "../../api";
import { ILoginRequest, IRefreshResponse } from "../../api/auth/types";
import { loginFailure, loginStart, loginSucess } from "./authReducer";
import { store } from "..";
import { AxiosPromise } from "axios";

export const loginUser = (data: ILoginRequest) => 
    async (dispatch: Dispatch<any>): Promise<void> => {
        try {
            dispatch(loginStart());

            const res = await api.auth.login(data);

            dispatch(loginSucess({ accessToken: res.data.access_token, refreshToken: res.data.refresh_token }));
        } catch (e: any) {
            console.error(e);
            dispatch(loginFailure('login error' + e.message));
        }
    }

export const refreshToken = () => 
    async (dispatch: Dispatch<any>): Promise<void> => {
        try {
            const refreshToken = store.getState().auth.authData.refreshToken;

            if (refreshToken !== null) {
                const res = await api.auth.refresh(refreshToken);

                dispatch(loginSucess({ accessToken: res.data.access_token, refreshToken: res.data.refresh_token }));
            } else {
                console.log('error refresh is null');
            }
        } catch (e: any) {
            console.log(e);
            dispatch(loginFailure('refresh error' + e.message))
        }
    }