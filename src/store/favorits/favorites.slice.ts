import { createSlice } from "@reduxjs/toolkit";

const initialState: any[] = [];

export const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    reducers: {
        toggleFavorites: (state, action) => {
            const recipe = action.payload;
            if (state.some(r => r.id === recipe.id))
                state = state.filter(r => r.id !== recipe.id)
            else
                state.push(recipe);
        }
    }
});

export const {actions, reducer} = favoritesSlice;